---
title: "Mentions Légales - En Construction"
---

The file **legal-brief.md** creates an internal link on the cover page without the leading icon.

If the parameter `detailed_page_homepage_content` is set to false neither this section nor the navigation menu entry will be shown (but the link on the cover page will be).

If `detailed_page_homepage_content` is set to true or is missing at all, the link, navigation menu and the content will be rendered. In that case this section might contain less or just summarized information compared to the single page referenced by the parameter `detailed_page_path`.


---
This file is a single page and is referenced by the button `Legal` on the cover start page.

It can contain more or additional information than the dedicated section on the homepage.

---

En vertu de l’article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l’économie numérique, il est précisé aux utilisateurs du site www.rajaobelina.re l’identité des différents intervenants dans le cadre de sa réalisation et de son suivi :

Propriétaire : Mathieu RAJAOBELINA
Créateur : Mathieu RAJAOBELINA
Responsable publication : Mathieu RAJAOBELINA – mathieu@rajaobelina.re Le responsable publication est une personne physique ou une personne morale.
Webmaster : Mathieu RAJAOBELINA – mathieu@rajaobelina.re 
Hébergeur : Framagit