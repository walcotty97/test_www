---
title: "Formations"
header_menu_title: "Formations"
navigation_menu_title: "Formations"
weight: 4
header_menu: true
---
### Stage PowerShell avancé
##### Juillet 2022
Orsys Formation, Arcueil (94)

### Licence Professionnelle Réseaux et Télécommunications
##### 2008 - 2011
IUT Saint-Pierre, La Réunion (974)
