---
title: "Compétences"
header_menu_title: "Compétences"
navigation_menu_title: "Compétences"
weight: 5
header_menu: true
---
|   **Administration**  |   **Langues** |
|:---------------       |:-----|
|   Windows Server 2012 à 2019             |   Français |
|   Debian/Ubuntu        |   Anglais |
|   Virtualisation  (Hyper V / ESXi) | 
|   Switchs (Cisco/HP/Aruba)             |
|   PowerShell          |
|   Bash                |
|   Office 365           |