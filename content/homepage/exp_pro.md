---
title: "Expériences professionnelles"
header_menu_title: "Expériences"
navigation_menu_title: "Expériences professionnelles"
weight: 3
header_menu: true
---
### Société de Presse de la Réunion 
###### Aou 2023 - Dec 2024, Sainte-Clotilde, Réunion
En charge de la mise en place du suivi du changement des pare feux, de la MCO/MCS du système d'informations. 

### Groupe Potier
###### Nov 2022 - Aout 2023, Le Port, Réunion
J'ai pu travailler sur le début de la mise en place d'une procédure d'arrivée et de départ des salariés du groupe. J'ai aussi mis en place le SSO pour GLPI.

### Armée de l'Air et de l'Espace
###### Nov 2017 - Nov 2022, Orléans, France
Engagé dans l'Armée de l'Air et de L'Espace pendant 5 ans, j'étais en charge du maintien en condition opérationnelle du SI de mon unité. Celui ci était notamment composé de serveurs Windows et Red Hat, de pare-feux Stormshield et de switchs de marques différentes. J'ai activement participer à la mise en conformité d'un SI classifié. J'ai pu être déployé en opérations extérieures. 

### Infobam
###### Aout 2017 - Oct 2017, Sainte-Clotilde, Réunion
J'assistais les utilsateurs de GBH. Une expérience courte mais enrichissante.

### Bee Technology
###### Juin 2010 - Mars 2017, Sainte-Clotilde, Réunion
J'étais en charge de la configuration et du déploiement des téléphones IP chez les clients B2B . La solution utilisée chez Bee Technology était de type IP Centrex.
