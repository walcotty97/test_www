---
title: "Crédits"
weight: last
header_menu: true
---
{{<icon class="fa fa-handshake-o ">}} Ressources utilisées pour générer ce site.

- {{<extlink text="Solid Computer - Fontawesome" href="https://fontawesome.com/icons/computer?f=classic&s=solid&pc=%23000000" icon="fa fa-external-link">}}
- {{<extlink text="Server - Pixabay" href="https://pixabay.com/fr/photos/serveur-espace-la-salle-des-serveurs-2160321/" icon="fa fa-external-link">}}
