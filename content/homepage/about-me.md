---
title: "A propos de moi"
weight: 1
header_menu: true
---

Je suis **administrateur systèmes et réseaux** à l'Ile de la Réunion. J'ai principalement évolué dans des environnement à majorité Windows mais préfére utiliser des solutions libres. J'aime aussi monter et tester des serveurs à la maison. Je m'essaye quelque fois au CTF sur Root-me.

Une phrase me caractérisant : Tout ce que tu fais deux fois scriptes le.

Je passe mon temps libre en famille, à jouer aux jeux vidéos (jeux de gestion/RPG), jouer au foot, lire et décourvir de nouvelles cultures.
